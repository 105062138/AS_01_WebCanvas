var canvas, canvasGrid, canvasTmp, ctx, ctxGrid, ctxTmp;
var curX = 0, curY = 0, preX = -1, preY = -1;
var curColorBox, curColor = '#000000', curWidth = 2;

var font = '50px Verdana';
var font_name = 'Verdana';
var hasInput = false;
var buffer = new Array();
var flagShape = false, flagTool = false;
var push_time = -1;


var currentTool = 'pencil';
var currentShape = '';
var currentShapeObj = null;


function drawIm(pic,n1,n2){
  return ctx.drawImage(pic, n1, n2);
}


function clearpad() {
  var canvas = document.querySelector('#canvas');
  var ctx = canvas.getContext("2d");
  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  var canvasTmp = document.querySelector('#canvasTmp');
  var ctxTmp = canvas.getContext("2d");
  ctxTmp.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctxTmp.fillStyle = "white";
  ctxTmp.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  var canvasGrid = document.querySelector('#canvas');
  var ctxGrid = canvas.getContext("2d");
  ctxGrid.clearRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);
  ctxGrid.fillStyle = "white";
  ctxGrid.fillRect(0, 0, window.innerWidth, window.innerHeight - document.getElementsByTagName('header')[0].offsetHeight);

}
function chooseShape(name) {
  ctxTmp.clearRect(0, 0, w, h);
  ctxTmp.beginPath();

  if (currentShapeObj != null) {
    currentShapeObj.draw(ctx);
  }

  currentTool = '';
  currentShape = name;
  canvasTmp.style.display = 'block';
  var link = document.getElementById(name).src;
  $('canvas').css('cursor', 'url(' + link + '), auto');
}

function chooseTool(name) {
  currentTool = name;
  currentShape = '';
  canvasTmp.style.display = 'none';
  var link = document.getElementById(name).src;
  $('canvas').css('cursor', 'url(' + link + '), auto');
  hasInput = true;
  if(currentTool=='text'){
    hasInput=false;
    canvas.onclick = function (e) {
      if (hasInput) return;
      addInput(e.clientX, e.clientY);
    }

  }

}
function undo(){
  if (push_time > 0) {
    push_time--;
    var Pic = new Image();
    Pic.src = buffer[push_time];
    Pic.onload = function () { ctx.drawImage(Pic, 0, 0); }
  }
}
function redo(){
  if (push_time < buffer.length - 1) {
    push_time++;
    var Pic = new Image();
    Pic.src = buffer[push_time];
    Pic.onload = function () { ctx.drawImage(Pic, 0, 0); }
  }
}
function changeThickness(value) {
  curWidth = Math.round(parseInt(value) / 10);
  if (curWidth == 0)
    curWidth = 1;
  document.getElementById('rangeDemo').style.height = curWidth + 'px';
  document.getElementById('rangeValue').innerHTML = curWidth + 'px';
}
function chooseColor(id) {
  $('#color-quick-1').removeClass('active');
  $('#color-quick-2').removeClass('active');
  $('#' + id).addClass('active');

  curColorBox = document.getElementById(id);
  curColor = curColorBox.style.background;
}

function pickColor(id) {
  curColor = id;
  curColorBox.style.background = id;
}


//
function createShape(point){
  switch (currentShape) {

    case 'ellipse':{
      currentShapeObj = new Ellipse(point, curWidth, curColor);
      break;
    }

    case 'equilateral':{
      currentShapeObj = new Equilateral(point, curWidth, curColor);
      break;
    }
    default:
  }
}
function handleShapeUp(e){
  
  flagShape = false;
  if (currentShape == 'curve' && currentShapeObj.currentMode() < 1){
    console.log(currentShapeObj.currentMode());
    return;
  }
  
  currentShapeObj.draw(ctx);
  currentShapeObj = null;
  cPush();
  
}
function handleShapeDown(e){
  curX = e.clientX - canvas.offsetLeft;
  curY = e.clientY - canvas.offsetTop;

  flagShape = true;

  canvasTmp.style.display = 'block';
  createShape(new Point(curX, curY));
}
function handleShapeMove(e){
  curX = e.clientX - canvas.offsetLeft;
  curY = e.clientY - canvas.offsetTop + 18;//height of icon

  if (flagShape){
    ctxTmp.clearRect(0, 0, w, h);
    ctxTmp.beginPath();
    if (currentShape == 'curve')
    {
      currentShapeObj.setMidPoint(new Point(curX,curY));
    }else
      currentShapeObj.setLastpoint(new Point(curX, curY));
    currentShapeObj.draw(ctxTmp);
  }
}

function addInput(x, y) {

  var input = document.createElement('input');
  font = curWidth * 10 + 'px ' + font_name;
  input.type = 'text';
  input.placeholder='Type Something.....';
  input.style.position = 'fixed';
  input.style.left = (x - 4) + 'px';
  input.style.top = (y - 4) + 'px';
  input.style.font = font;
  input.style.color = curColor.color;
  input.onkeydown = handleEnter;
  document.body.appendChild(input);
  input.focus();
  hasInput = true;
}
function init() {
  canvas = document.getElementById('canvas');
  canvasGrid = document.getElementById('canvasGrid');
  canvasTmp = document.getElementById('canvasTmp');

  canvas.width = 1065;
  canvas.height = 600;
  canvasGrid.width = canvas.width;
  canvasGrid.height = canvas.height;
  canvasTmp.width = canvas.width;
  canvasTmp.height = canvas.height;

  ctx = canvas.getContext('2d');
  ctxGrid = canvasGrid.getContext('2d');
  ctxTmp = canvasTmp.getContext('2d');

  w = canvas.width;
  h = canvas.height;
  
  canvas.addEventListener('mousedown', function (e) { handleToolDown(e) }, false);
  canvas.addEventListener('mouseup', function (e) { handleToolUp(e) }, false);
  canvas.addEventListener('mousemove', function (e) { handleToolMove(e) }, true);

  canvasTmp.addEventListener('mousedown', function (e) { handleShapeDown(e) }, false);
  canvasTmp.addEventListener('mouseup', function (e) { handleShapeUp(e)}, false);
  canvasTmp.addEventListener('mousemove', function (e) { handleShapeMove(e) }, false);
  
  
  canvasTmp.style.display = 'none';
  ctx.clearRect(0, 0, w, h);
  ctx.beginPath();
  ctxGrid.clearRect(0, 0, w, h);
  ctxGrid.beginPath();
  ctxGrid.rect(0, 0, w, h);
  ctxGrid.fillStyle = '#FFFFFF';
  ctxGrid.fill();
  document.getElementById("fileUpload").addEventListener("change",readImage,false);//
  ctx.clearRect(0, 0, w, h);
  ctx.fillStyle = "white";
  ctx.fillRect(0, 0, w, h);
  ctxTmp.clearRect(0, 0, w,h);
  ctxTmp.fillStyle = "white";
  ctxTmp.fillRect(0, 0, w,h);
  cPush();
  
  $('canvas').css('cursor', 'url(./pencil.png), auto');
  curColorBox = document.getElementById('color-quick-1');
  
}



function cPush() {
  ++push_time;
  if (push_time < buffer.length) { buffer.length = push_time; }
  buffer.push(document.getElementById('canvas').toDataURL());
}