function handleToolDown(e){
  flagTool = true;
  
  preX = curX;
  preY = curY;
  if (currentTool == 'eraser'){
    ctx.globalCompositeOperation = 'destination-out';
  }
  drawing = true;
}
function handleToolUp(e){
  
  flagTool = false;
  
  preX = -1;
  preY = -1;
  if (currentTool == 'eraser'){
    ctx.globalCompositeOperation = 'source-over';
  }
  cPush();//define in setting
  drawing = false;
}

function handleToolMove(e){
  curX = e.clientX;
  curY = e.clientY;
  if (currentTool == 'pencil' && flagTool || currentTool == 'eraser' && flagTool){
    ctx.beginPath();
    ctx.lineWidth = curWidth;
    ctx.strokeStyle = curColor;
    ctx.moveTo(preX, preY);
    ctx.lineTo(curX, curY);
    ctx.stroke();
    preX = curX;
    preY = curY;
  }
}



function drawText(txt, x, y) {
  ctx.textBaseline = 'top';
  ctx.textAlign = 'left';
  ctx.fillStyle = curColor;
  ctx.font = font;
  ctx.fillText(txt, x-190, y - 130);
  cPush();
}
function change_font(f) {
  if (f == 0) {
    font_name = 'Verdana';
  }
  else if (f == 1) {
    font_name = '微軟正黑體';
  }
  else if (f == 2) {
    font_name = '新細明體';
  }
  else if (f == 3) {
    font_name = '標楷體';
  }
}
function handleEnter(e) {
  var keyCode = e.keyCode;
  if (keyCode === 13) {
    drawText(this.value, parseInt(this.style.left, 10), parseInt(this.style.top, 10));
    document.body.removeChild(this);
    hasInput = false;
  }
}

function readImage(){//
  canvas = document.getElementById('canvas');

  canvas.width = 1065;
  canvas.height = 600;
  canvasGrid.width = canvas.width;
  canvasGrid.height = canvas.height;
  canvasTmp.width = canvas.width;
  canvasTmp.height = canvas.height;
  //haha
  ctx = canvas.getContext('2d');
  ctxGrid = canvasGrid.getContext('2d');
  ctxTmp = canvasTmp.getContext('2d');
  w = canvas.width;
  h = canvas.height;
  if(this.files&&this.files[0]){
    ctx.clearRect(0, 0,w, h);
    clearpad();
    var FR = new FileReader();
    FR.onload = function(event) {
      var img = new Image();
      img.addEventListener("load", function() {
        ctx.drawImage(img, 0, 0,w, h);
        cPush();
      });
      img.src = event.target.result;
    };       
    FR.readAsDataURL( this.files[0] );
  }
}

function upLo() 
{ 
  fileUpload.click();   
  textfield.value = upload.value;   
}
function download(){ 
  var canvas=document.querySelector('#canvas'); 
  var ctx=canvas.getContext("2d"); 
  var base64Img = canvas.toDataURL(); 
  var oA = document.createElement('a'); 
  oA.href = base64Img; 
  oA.download = 'picture.png'; 

  var event = document.createEvent('MouseEvents'); 
  event.initMouseEvent('click', true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null); 
  oA.dispatchEvent(event); 
};

